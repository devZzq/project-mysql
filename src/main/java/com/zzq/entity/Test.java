package com.zzq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@TableName("table_test")
public class Test {

    @TableId(value = "test_id", type = IdType.ASSIGN_ID)
    private Long testId;

    @TableField("username")
    private String username;

    @TableField("age")
    private Integer age;



}
