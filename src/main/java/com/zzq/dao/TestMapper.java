package com.zzq.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zzq.entity.Test;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TestMapper extends BaseMapper<Test> {
}
