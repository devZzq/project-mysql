package com.zzq.controller;


import com.google.gson.Gson;
import com.zzq.dto.TestRequest;
import com.zzq.entity.Test;
import com.zzq.service.TestService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class TestController {

    private final TestService testService;

    @PostMapping("/insertData")
    public String insertData(@RequestBody TestRequest testRequest) {
        testService.save(new Test().setAge(testRequest.getAge()).setUsername(testRequest.getUsername()));
        return "insert successMaster";
    }

    @GetMapping("/getDataList")
    public String getData() {
        List<Test> list = testService.list();
        return new Gson().toJson(list);
    }

    @DeleteMapping("/deleteById/{testId}")
    public String insertData(@PathVariable Long testId) {
        testService.removeById(testId);
        return "delete success";
    }


}
