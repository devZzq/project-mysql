package com.zzq.dto;

import lombok.Data;


@Data
public class TestRequest {

    private String username;
    private Integer age;
}
